package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class NoReturnControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_200_when_no_return() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_204() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().isNoContent());
    }
}
