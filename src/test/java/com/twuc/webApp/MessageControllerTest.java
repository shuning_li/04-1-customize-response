package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_string() throws Exception {
        mockMvc.perform(get("/api/messages/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_get_message_obj() throws Exception {
        mockMvc.perform(get("/api/message-objects/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"hello\"}"));
    }

    @Test
    void should_get_message_obj_with_annotation() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/hello"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"value\":\"hello\"}"));
    }

    @Test
    void should_get_message_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{ \"value\": \"{hello}\" }"))
                .andExpect(header().string("X-Auth", "me"));
    }
}
