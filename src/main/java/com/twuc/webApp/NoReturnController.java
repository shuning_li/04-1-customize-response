package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class NoReturnController {
    @GetMapping("/no-return-value")
    public void getNothing() {}

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @GetMapping("/no-return-value-with-annotation")
    public void getNothingWithAnnotation() {

    }
}
