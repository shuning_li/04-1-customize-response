package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MessageController {
    @GetMapping("/messages/{message}")
    public String getMessage(@PathVariable String message) {
        return message;
    }

    @GetMapping("/message-objects/{message}")
    public Message getMessageObj(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageObjWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    public ResponseEntity<String> getMessageEntity(@PathVariable String message) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-Auth", "me")
                .body("{ \"value\": \"{" + message + "}\" }");
    }
}

class Message {
    private String value;

    public Message(String message) {
        this.value = message;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}